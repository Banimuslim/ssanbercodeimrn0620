// Soal 1 - If Else, Game Werewolf
console.log("=======================")
var nama = "John"
var peran = "Guard"

if (nama === "") {
    console.log("Nama harus diisi!")
}
else {
    console.log("Selamat datang di Dunia Werewolf, " + nama + ". Pilih peranmu untuk memulai game")
    if (peran === "Penyihir") {console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf")} 
    if (peran === "Guard") {console.log("Halo guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf")} 
    if (peran === "Werewolf"){console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam")}
}
    console.log("")

// Soal 2 - Switch case, Kalender
console.log("=======================")
var tanggal = 21
var bulan = 1
var tahun = 1945

if (tanggal < 31){console.log(tanggal)} else {console.log("(Input angka antara 1-31)")}

switch (bulan) {
    case 1: console.log("Januari"); break;
    case 2: console.log("Februari"); break;
    case 3: console.log("Maret"); break;
    case 4: console.log("April"); break;
    case 5: console.log("Mei"); break;
    case 6: console.log("Juni"); break;
    case 7: console.log("Juli"); break;
    case 8: console.log("Agustus"); break;
    case 9: console.log("September"); break;
    case 10: console.log("Oktober"); break;
    case 11: console.log("Nopember"); break;
    case 12: console.log("Desember"); break;
    default: console.log("(Input angka antara 1-12)"); break;}
 
if(tahun>1900 && tahun<2200){console.log(tahun)} else {console.log("(Angka diluar range)")}
console.log("")

// Versi lain soal 2
console.log("=======================")
console.log("versi lain yg lebih rapi")

if(tanggal>=1 && tanggal <=31){
    if(tahun>=1900 && tahun<=2200){
        switch(bulan){
            case 1: teksbulan = "Januari"; break;
            case 2: teksbulan = "Februari"; break;
            case 3: teksbulan = "Maret"; break;
            case 4: teksbulan = "April"; break;
            case 5: teksbulan = "Mei"; break;
            case 6: teksbulan = "Juni"; break;
            case 7: teksbulan = "Juli"; break;
            case 8: teksbulan = "Agustus"; break;
            case 9: teksbulan = "September"; break;
            case 10: teksbulan = "Oktober"; break;
            case 11: teksbulan = "Nopember"; break;
            case 12: teksbulan = "Desember"; break;
            default: teksbulan = "(Input angka antara 1-12)"; break;
        }
    }
}console.log (tanggal, " ", teksbulan, " ", tahun)