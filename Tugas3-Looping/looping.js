// Soal 1 - While looping
console.log("Soal 1 - While looping")
var angka = 2
while (angka <=20){
    console.log(angka, ' - I love coding')
    angka+=2
}
console.log ("")

var angka2 = 20
while (angka2 > 0){
    console.log(angka2, ' - I will become a mobile developer')
    angka2-=2
}
console.log("================================")
console.log("")

// Soal 2 - For looping dengan syarat
console.log("Soal 2 - For looping dengan syarat")
var angka3=1
for(var angka4=angka3; angka4<=20; angka4++) {
    if((angka4%3)===0 && (angka4%2)!==0){
        console.log(angka4, ' - I love coding')
    }else if((angka4%2)===0){
        console.log(angka4, ' - Berkualitas');
    }else if((angka4%2)!==0){
        console.log(angka4, ' - Santai')
    }
}
console.log("================================")
console.log("")

// Soal 3 - Membuat Persegi Panjang '#'
console.log("Soal 3 - Membuat Persegi Panjang #")
var pagar1 = '########'
var jumlah = 1
var baris = 4
while(jumlah<=baris){
    console.log(pagar1)
    jumlah++
}
console.log("================================")
console.log("")

// Soal 4 - Membuat Tangga
console.log(" Soal 4 - Membuat Tangga")
var pagar2=""
for(var angka5 = 1; angka5 <= 7; angka5++){
    pagar2 = pagar2 + '#'
    console.log(pagar2)
}
console.log("================================")
console.log("")

// Soal 5 - Membuat Papan Catur
console.log("Soal 5 - Membuat Papan Catur")
for(i=1; i<=8; i++){
    if((i%2)!==0){
        console.log(' # # # #')
    }else console.log('# # # #')
}