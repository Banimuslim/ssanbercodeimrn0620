// Soal No.1
console.log("Soal No.1 - Function teriak()")
console.log("")
function teriak(){
    var kata = '"Halo Sanbers!"'
    return kata
}
console.log(teriak())

console.log("")
console.log("=========================")
console.log("")

// Soal No.2
console.log("Soal No.2 - Function kalikan()")
console.log("")

function kalikan(num1, num2){
    var x = num1*num2
    return x
}

var num1 = 12
var num2 = 4

var hasilkali = kalikan(num1, num2)
console.log(hasilkali)

console.log("")
console.log("=========================")
console.log("")

// Soal No.3
console.log("Soal No.3 - Function Introduce()")
console.log("")

function introduce(name, age, address, hobby){
    var y = "Nama saya " + name + ", umur saya " + age +
     " tahun, alamat saya di " + address 
     + ", dan saya punya hobby yaitu " + hobby
     return y
}

var perkenalan = introduce("agus", 30, "Jln. Malioboro, Yogyakarta", "Gaming")
console.log(perkenalan)

console.log("")
console.log("=========================")
console.log("")